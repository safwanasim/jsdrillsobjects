function mapObject(obj, cb) {
    // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
    // http://underscorejs.org/#mapObject
    for (let property in obj) {
        obj[property] = cb(obj[property])
    }
    return obj
}

module.exports = mapObject