function pairs(obj) {
    // Convert an object into a list of [key, value] pairs.
    // http://underscorejs.org/#pairs
    let resultArray = []
    for (let object in obj) {
        resultArray.push([object, obj[object]])
    }
    return resultArray

}
module.exports = pairs