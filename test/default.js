function defaults(obj, defaultProps) {
    // Fill in undefined properties that match properties on the `defaultProps` parameter object.
    // Return `obj`.
    // http://underscorejs.org/#defaults
    for (let property in obj) {
        if (obj[property] == undefined) {
            if (defaultProps[property]) {
                obj[property] = defaultProps[property]
            }
        }
    }
    return obj
}

module.exports = defaults