const invert = require("../invert");

const testObject = {
    name: 'Bruce Wayne',
    age: 36,
    location: 'Gotham'
};
const actualOutput = invert(testObject)
console.log(testObject, actualOutput)

for (let object in testObject) {
    if (!testObject[object] in actualOutput) { //if object not present, then test failed
        console.log("Test Failed")
        return
    }
}
console.log("Test Passed")