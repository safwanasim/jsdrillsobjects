const defaults = require("./default");

const testObject = {
    name: 'Bruce Wayne',
    age: 36,
    location: 'Gotham',
    movie: undefined,
    actor: undefined,
    producer: undefined
};

const defaultProps = {
    movie: 'The Dark Knight',
    actor: 'Joker',
    producer: 'Chris Nolan'
}

const actualOutput = defaults(testObject, defaultProps)
console.log(actualOutput)
const expectedOutput = {
    name: 'Bruce Wayne',
    age: 36,
    location: 'Gotham',
    movie: 'The Dark Knight',
    actor: 'Joker',
    producer: 'Chris Nolan'
}

if (JSON.stringify(expectedOutput) == JSON.stringify(actualOutput)) {
    console.log("Test Passed")
} else {
    console.log("Test Failed")
}