const keys = require("../keys");

const testObject = {
    name: 'Bruce Wayne',
    age: 36,
    location: 'Gotham'
};

const expectedOutput = Object.keys(testObject)
const actualOutput = keys(testObject)

if (JSON.stringify(expectedOutput) == JSON.stringify(actualOutput)) {
    console.log("Test Passed")
} else {
    console.log("Test Failed")
}