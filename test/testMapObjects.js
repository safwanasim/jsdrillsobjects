const mapObject = require("../mapObjects");

const testObject = {
    name: 'Bruce Wayne',
    age: 36,
    location: 'Gotham'
};

function callback(value) {
    return value + value
}


const expectedOutput = Object.values(testObject).map((value) => {
    return (value + value)
})
const actualOutput = Object.values(mapObject(testObject, callback))


console.log(expectedOutput, actualOutput)

if (JSON.stringify(expectedOutput) == JSON.stringify(actualOutput)) {
    console.log("Test Passed")
} else {
    console.log("Test Failed")
}