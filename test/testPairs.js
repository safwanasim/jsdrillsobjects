const pairs = require("../pairs");

const testObject = {
    name: 'Bruce Wayne',
    age: 36,
    location: 'Gotham'
};

const expectedOutput = Object.entries(testObject)
const actualOutput = pairs(testObject)

console.log(expectedOutput, actualOutput)


if (JSON.stringify(expectedOutput) == JSON.stringify(actualOutput)) {
    console.log("Test Passed")
} else {
    console.log("Test Failed")
}