const values = require("../values");

const testObject = {
    name: 'Bruce Wayne',
    age: 36,
    location: 'Gotham'
};

const expectedOutput = Object.values(testObject)
const actualOutput = values(testObject)

if (JSON.stringify(expectedOutput) == JSON.stringify(actualOutput)) {
    console.log("Test Passed")
} else {
    console.log("Test Failed")
}